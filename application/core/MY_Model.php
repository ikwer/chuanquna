<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of My_Model
 *
 * @author demon
 */
class MY_Model extends CI_Model {

    var $table = 'db_name';

    public function __construct($table = '')
    {
        parent::__construct();
        $this->table = $table;
    }

    /**
     * 列表信息
     * @param cloums
     * @param where
     * @param page
     * @param row
     * @return array
     */
    public function getList($colums = '*', $where = array(), $page = 1, $row = 10) {
        $index = ($page - 1) * $row;
        $res = $this->db->select($colums)
                ->from($this->table)
                ->where($where)
                ->limit($row, $index)
                ->get();
        return $res->result_array();
    }

    /**
     * 总数
     * @return array
     */
    public function getCount() {
        return $this->db->count_all_results($this->table);
    }

    /**
     * 插入
     * @param data
     * @return array
     */
    public function insert($data) {
        $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() > 0) ? $this->db->insert_id() : FALSE;
    }

    /**
     * 更新
     * @param data
     * @return array
     */
    public function update($data, $where) {
        return $this->db->count_all_results($this->table);
    }
}
