<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminUser');
	}

	public function index()
	{
		$str_pool = '3456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXY';
		$length = 4;
		$word = '';
		for ($i = 0; $i < $length; $i++) {
			$word .= $str_pool[mt_rand(0, strlen($str_pool) - 1)];
		}
		$data['word'] = $word;
		$this->session->set_userdata('admin_captcha', $word);
		$this->load->view('admin/login/index', $data);
	}

	public function login()
	{
		$data = $this->input->post();
		if (strtolower($data['captcha']) != strtolower($this->session->userdata('admin_captcha'))) {
			ajax_return(2, '验证码不正确');
		}
		$this->session->unset_userdata('admin_captcha');
		$user = $this->AdminUser->getUserByUsername($data['username']);
		if (!$user) {
			ajax_return(3, '用户名不存在');
		}
		if ($user['password'] != md5($data['password'])) {
			ajax_return(4, '密码错误');
		}
		unset($user['password']);
		$this->session->set_userdata('admin_user', $user);
		ajax_return(1, '登录成功');
	}

}
