<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('AdminMenu');
	}

	public function index()
	{
		$admin_user = $this->session->userdata('admin_user');
		if (!$admin_user) {
			redirect('/admin/login');
		}
		$data['user'] = $admin_user;
		$data['menu'] = $this->AdminMenu->getMenu();
		$this->load->view('admin/layout/main', $data);
	}

}
