<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AdminUser');
		$this->load->model('AdminRole');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('admin/admin/index');
	}

	public function datalist()
	{
		$request = $this->input->get();
		$page = $request['page'];	
		$limit = $request['limit'];
		$total = $this->AdminUser->getCount();
		$data = $this->AdminUser->getList('*', [], $page, $limit);
		echo json_encode(array('code'=> 0, 'msg' => '查询成功', 'count' => $total, 'data' => $data));
		exit;
	}

	public function add()
	{
		$data['role'] = $this->AdminRole->getListForTree();
		$this->load->view('admin/admin/add', $data);
	}

	public function save()
	{
		$data = $this->input->post();
		//表单验证规则
		$this->form_validation->set_rules('username', '用户名', 'required');
		$this->form_validation->set_rules('nickname', '用户昵称', 'required');
        $this->form_validation->set_rules('password', '密码', 'required');
        $this->form_validation->set_rules('password2', '确认密码', 'required|matches[password]');
        //自定义提示语
        $this->form_validation->set_message('required', '必须填写');
		$this->form_validation->set_message('matches', '两次密码不一致');
		//验证表单
        if (!$this->form_validation->run()) {
        	//返回错误提示
        	ajax_return(-1, validation_errors());
        }
        //判断用户是否存在
        if ($this->AdminUser->getUserByUsername($data['username'])) {
        	ajax_return(-1, '用户已经存在');
        }
    	//去掉password2
    	unset($data['password2']);
    	//加上当前时间
    	$data['dateline'] = date("Y-m-d H:i:s");
    	if ($this->AdminUser->insert($data)) {
    		ajax_return(1, '提交成功');
    	}
    	ajax_return(-1, '系统服务器错误，请稍后再试');
	}

}
