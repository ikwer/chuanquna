<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array('title' => '测试数据');
		ajax_return(1, '请求成功', $data);
	}

}
