<?php

/**
 * Description of admin_role
 *
 * @author demon
 */
class AdminRole extends MY_Model {

    const TABLE = 'admin_role';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }

    /**
     * 下拉框规则列表
     * @return array
     */
    public function getListForTree() {
        $res = $this->db->select('id,name')
                ->from(self::TABLE)
                ->get();
        return $res->result_array();
    }

}
