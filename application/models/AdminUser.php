<?php

/**
 * Description of admin_user
 *
 * @author demon
 */
class AdminUser extends MY_Model {

    const TABLE = 'admin_user';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }

    /**
     * 查询用户
     * @param username
     * @return array
     */
    public function getUserByUsername($username) {
    	$sql = 'SELECT `id`,`password`,`nickname` FROM '. self::TABLE. ' WHERE `username` = ?';
        return $this->db->query($sql, array($username))->row_array();
    }

}
