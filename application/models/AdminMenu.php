<?php

/**
 * Description of admin_menu
 *
 * @author demon
 */
class AdminMenu extends MY_Model {

    const TABLE = 'admin_menu';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }

    /**
     * 菜单信息
     * @return array
     */
    public function getMenu() {
        $res = $this->db->query('SELECT * FROM '.self::TABLE)->result_array();
        return $menu = $this->getTree($res, 0);
    }

    /**
     * 菜单递归函数
     * @param data
     * @param pid
     * @return array
     */
    private function getTree($data, $pId)
	{
		$tree = '';
		foreach($data as $k => $v)
		{
		    if($v['pid'] == $pId)
		    {         
		   		$v['child'] = $this->getTree($data, $v['id']);
		    	$tree[] = $v;
		    }
		}
		return $tree;
	}

}
