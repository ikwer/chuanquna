﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=emulateIE7" />
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_PATH;?>admin/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo STATIC_PATH;?>admin/css/skin_/login.css" />
<script type="text/javascript" src="<?php echo STATIC_PATH;?>admin/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo STATIC_PATH;?>admin/js/jquery.select.js"></script>
<title>用户登录</title>
</head>

<body>
<div id="container">
    <div id="bd">
    	<div id="main">
        	<div class="login-box">
                <div id="logo"></div>
                <h1></h1>
                <div class="input username" id="username">
                    <label for="userName">用户名</label>
                    <span></span>
                    <input type="text" id="userName" />
                </div>
                <div class="input psw" id="psw">
                    <label for="password">密&nbsp;&nbsp;&nbsp;&nbsp;码</label>
                    <span></span>
                    <input type="password" id="password" />
                </div>
                <div class="input validate" id="validate">
                    <label for="valiDate">验证码</label>
                    <input type="text" id="valiDate" />
                    <div class="value"><?php echo $word;?></div>
                </div>
                <div id="btn" class="loginButton">
                    <input type="button" class="button" value="登录"  />
                </div>
            </div>
        </div>
    </div>
   
</div>

</body>
<script type="text/javascript">
	var height = $(window).height() > 445 ? $(window).height() : 445;
	$("#container").height(height);
	var bdheight = ($(window).height() - $('#bd').height()) / 2 - 20;
	$('#bd').css('padding-top', bdheight);
	$(window).resize(function(e) {
        var height = $(window).height() > 445 ? $(window).height() : 445;
		$("#container").height(height);
		var bdheight = ($(window).height() - $('#bd').height()) / 2 - 20;
		$('#bd').css('padding-top', bdheight);
    });
	$('select').select();
	
	$('.loginButton').click(function(e) {
        var username = $("#userName").val();
        if (!username) {
            alert('用户名不能为空')
            return false;
        }
        var password = $("#password").val();
        if (!password) {
            alert('密码不能为空')
            return false;
        }
        var valiDate = $("#valiDate").val();
        if (!valiDate) {
            alert('验证码不能为空')
            return false;
        }
        $.ajax({
            url : '<?php echo BASE_URL?>/admin/login/login',
            type: 'POST',
            data: {"username": username, "password": password, "captcha": valiDate},
            dataType: 'json',
            success: function(data) {
                if (data.code == 1) {
                    window.location.href='<?php echo BASE_URL?>/admin/home/index';
                } else {
                    alert(data.message)
                    window.location.reload();
                }
            }
        })
    });
</script>

</html>
