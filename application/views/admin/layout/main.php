<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>思侬后台管理</title>
    <link rel="stylesheet" href="<?php echo STATIC_PATH;?>layui/css/layui.css">
    <script src="<?php echo STATIC_PATH;?>admin/js/jquery.js"></script>
    <script src="<?php echo STATIC_PATH;?>layui/layui.js"></script>
</head>
<body>
    <div class="layui-layout-admin">
        <!--头部导航栏-->
        <div class="layui-header">
            <div class="layui-logo">思侬后台管理</div>
            <ul class="layui-nav layui-layout-left">
                <!-- <li class="layui-nav-item"><a href="javascript:void(0)">控制台</a></li>
                <li class="layui-nav-item"><a href="javascript:;">商品管理</a></li>
                <li class="layui-nav-item">
                    <a href="javascript:;">其他系统</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">邮件管理</a></dd>
                        <dd><a href="javascript:;">消息管理</a></dd>
                        <dd><a href="javascript:;">授权管理</a></dd>
                    </dl>
                </li> -->
            </ul>
            <ul class="layui-nav layui-layout-right">
                <li class="layui-nav-item">
                    <a href="">
                        <img src="https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqoibibZCpRw5Eiaia1JQFgFGsZdiaBlCGlxHBnHyibfMTK1fwxBLGpyxqeqlW0RBqeBicuiaibTIJuibttLxSw/132" class="layui-nav-img">
                        <?php echo $user['nickname'];?>
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="">基本资料</a></dd>
                        <dd><a href="">安全设置</a></dd>
                    </dl>
                </li>
            </ul>
        </div>

        <!--左侧导航栏-->
        <div class="layui-side layui-bg-black">
            <div class="layui-side-scroll">
                <ul class="layui-nav layui-nav-tree" lay-filter="hbkNavbar">
                    <?php for ($i = 0; $i < count($menu); $i ++): ?>
                        <li class="layui-nav-item">
                            <a href="javascript:;"><?php echo $menu[$i]['name']; ?></a>
                            <dl class="layui-nav-child">
                                
                                  <?php for ($j = 0; $j < count($menu[$i]['child']); $j ++): ?>
                                      <dd><a href="javascript:void(0);" data-id="<?php echo $menu[$i]['child'][$j]['name']; ?>" data-url="<?php echo $menu[$i]['child'][$j]['url']; ?>"><?php echo $menu[$i]['child'][$j]['name']; ?></a></dd>
                                  <?php endfor;?>
                                
                            </dl>
                        </li>
                    <?php endfor;?>
                </ul>
            </div>
        </div>

        <!--中间主体-->
        <div class="layui-body" id="container">
            <div class="layui-tab" lay-filter="tabs" lay-allowClose="true">
                <ul class="layui-tab-title">
                    <li class="layui-this">首页</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">首页内容</div>
                </div>
            </div>
        </div>

        <!--底部-->
        <div class="layui-footer">
            <center>Power By Demon</center>
        </div>
    </div>
<script>
    //JavaScript代码区域
    layui.use('element', function(){
        var element = layui.element;
        /*使用下面的方式需要引用jquery*/
        $('.layui-nav-child a').click(function () {
            var url = $(this).data('url');
            var title = $(this).html();
            var id = $(this).data('id');
            var exist=$("li[lay-id='"+id+"']").length; //判断是否存在tab
            if(exist==0){
                element.tabAdd('tabs',{
                    id : id,
                    title : title,
                    content : '<iframe scrolling="auto" frameborder="0"  src="<?php echo BASE_URL?>'+url+'" style="width:100%;height:100%;"></iframe>'             
                });     
            }
            element.tabChange('tabs', id);  
            var headerH = $('.layui-header').height();
            var tabH = $("li[lay-id='"+id+"']").height();
            var footerH = $('.layui-footer').height();
            var tabTH = $(".layui-tab-title").height();
            $('.layui-tab-content').height($(window).height()-headerH-tabH-footerH-tabTH);
            $('.layui-show').height($(window).height()-headerH-tabH-footerH-tabTH);
        });

        
    });

    function closeTab(tabname){
        var element = layui.element;
        element.tabDelete('tabs', tabname);  
    }

    function openTab(tabname, url){
        var element = layui.element;
        var exist=$("li[lay-id='"+tabname+"']").length;
        if(exist==0){
            element.tabAdd('tabs',{
                id : tabname,
                title : tabname,
                content : '<iframe scrolling="auto" frameborder="0"  src="<?php echo BASE_URL?>'+url+'" style="width:100%;height:100%;"></iframe>'             
            });     
        }
        element.tabChange('tabs', tabname);  
        var headerH = $('.layui-header').height();
        var tabH = $("li[lay-id='"+tabname+"']").height();
        var footerH = $('.layui-footer').height();
        var tabTH = $(".layui-tab-title").height();
        $('.layui-tab-content').height($(window).height()-headerH-tabH-footerH-tabTH);
        $('.layui-show').height($(window).height()-headerH-tabH-footerH-tabTH);
    }
    

</script>
</body>
</html>
  