<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>添加管理员</title>
<link rel="stylesheet" href="<?php echo STATIC_PATH;?>layui/css/layui.css" media="all">
<script src="<?php echo STATIC_PATH;?>admin/js/jquery.js"></script>
<script src="<?php echo STATIC_PATH;?>layui/layui.js"></script>
</head>
<body>
<form class="layui-form" method="post"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
  <div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-inline">
      <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input" lay-verify="required" value="<?php echo isset($username)? $username : '';?>">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">用户昵称</label>
    <div class="layui-input-inline">
      <input type="text" name="nickname" placeholder="请输入" autocomplete="off" class="layui-input" lay-verify="required" value="<?php echo isset($nickanme)? $nickanme : '';?>">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">密码</label>
    <div class="layui-input-inline">
      <input type="password" name="password" placeholder="请输入" autocomplete="off" class="layui-input" lay-verify="required" value="<?php echo isset($password)? $password : '';?>">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">确认密码</label>
    <div class="layui-input-inline">
      <input type="password" name="password2" placeholder="请输入" autocomplete="off" class="layui-input" lay-verify="required" value="<?php echo isset($password)? $password : '';?>">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">用户组</label>
    <div class="layui-input-inline">
      <select name="role_id" lay-verify="required">  
        <option value="0">无权限</option>
        <?php for($i = 0; $i < count($role); $i ++): ?>
        <option <?php echo isset($role_id) && $role_id == $role[$i]['id'] ? 'selected=selected' : '';?> value="<?php echo $role[$i]['id'];?>"><?php echo $role[$i]['name'];?></option>
        <?php endfor;?>
      </select>
    </div>
  </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
    	<input type="hidden" name="id" value="<?php echo isset($id)? $id : 0;?>">
      <button class="layui-btn" lay-submit lay-filter="*">立即提交</button>
      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
    </div>
  </div>
  <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 -->
</form>

<script>
layui.use('form', function(){
  var form = layui.form;
  
  //各种基于事件的操作，下面会有进一步介绍
  form.on('submit(*)', function(data){
	  $.ajax({
            url : '<?php echo BASE_URL?>/admin/admin/save',
            type: 'POST',
            data: data.field,
            dataType: 'json',
            success: function(data) {
                if (data.code == 1) {
                    layer.alert('提交成功')
                    window.parent.openTab('管理员列表', '<?php echo BASE_URL?>/admin/admin/index');
                    window.parent.closeTab('添加管理员');  
                } else {
                    layer.alert(data.message)
                }
            }
        })
	  return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
	});
});


</script>
</body>
</html>