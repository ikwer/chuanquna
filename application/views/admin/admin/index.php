<link rel="stylesheet" href="<?php echo STATIC_PATH;?>layui/css/layui.css" media="all">
<script src="<?php echo STATIC_PATH;?>admin/js/jquery.js"></script>
<script src="<?php echo STATIC_PATH;?>layui/layui.js"></script>

<table class="layui-table" lay-data="{url:'<?php echo BASE_URL?>/admin/admin/datalist', page:true, id:'idTest'}" lay-filter="demo">
  <thead>
    <tr>
      <th lay-data="{field:'id', sort: true, fixed: true}">ID</th>
      <th lay-data="{field:'username'}">用户名</th>
      <th lay-data="{field:'password', sort: true}">密码</th>
      <th lay-data="{field:'nickname'}">昵称</th>
      <th lay-data="{field:'dateline'}">创建时间</th>
      <th lay-data="{fixed:'right', width:178, align:'center', toolbar: '#barDemo'}"></th>
    </tr>
  </thead>
</table>
 
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
                     
<script>
layui.use('table', function(){
  var table = layui.table;
  //监听表格复选框选择
  table.on('checkbox(demo)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(demo)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      layer.msg('ID：'+ data.id + ' 的查看操作');
    } else if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        obj.del();
        layer.close(index);
      });
    } else if(obj.event === 'edit'){
      layer.alert('编辑行：<br>'+ JSON.stringify(data))
    }
  });
});
</script>