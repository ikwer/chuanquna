<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/array_helper.html
 */
// ------------------------------------------------------------------------

/**
 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
 * 注意：服务器需要开通fopen配置
 * @param $word 要写入日志里的文本内容 默认值：空值
 */
function logResult($word = '') {
    $fp = fopen("postlog.txt", "a");
    flock($fp, LOCK_EX);
    $url = 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    fwrite($fp, "执行日期：" . strftime("%Y%m%d%H%M%S", time()) . "----" . $url . "\n" . $word . "\n");
    flock($fp, LOCK_UN);
    fclose($fp);
}

//生成加密后的唯一token
if (!function_exists('settoken')) {

    function settoken($uid) {
        $str = md5($uid . microtime(true));
        return $str;
    }

}

/**
 * $code 返回的提示码 0：成功 ,失败
 * $message 返回的提示信息
 * $data 返回的信息
 */
if (!function_exists('ajax_return')) {

    function ajax_return($code = -1, $mag = '', $data = null) {
        if (!is_numeric($code)) {
            return 'code error';
        }
        if($data==null || empty($data))
        {
            $result = array(
                'code' => $code,
                'message' => $mag
            );
        }
        else
        {
            $result = array(
                'code' => $code,
                'message' => $mag,
                'data'=>$data
            );
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

}

/*
 * 生成签名，$args为请求参数，$key为私钥
 */
if (!function_exists('makeSignature')) {

    function makeSignature($args, $key) {
        ksort($args);
        $requestString = '';
        foreach ($args as $k => $v) {
            $requestString .= $k . '=' . urlencode($v);
        }
        $newSign = hash_hmac("md5", strtolower($requestString), $key);
        return $newSign;
    }

}

/**
 * 根据经纬度获取在范围坐标的数据
 * @param lat 纬度 lon 经度 raidus 单位米
 * return minLat,minLng,maxLat,maxLng
 */
if (!function_exists('GetRange')) {

    function GetRange($lat, $lon, $raidus) {
        $PI = 3.14159265;

        $latitude = $lat;
        $longitude = $lon;

        $degree = (24901 * 1609) / 360.0;
        $raidusMile = $raidus;

        $dpmLat = 1 / $degree;
        $radiusLat = $dpmLat * $raidusMile;
        $minLat = $lat - $radiusLat;
        $maxLat = $lat + $radiusLat;

        $mpdLng = $degree * cos($latitude * ($PI / 180));
        $dpmLng = 1 / $mpdLng;
        $radiusLng = $dpmLng * $raidusMile;
        $minLng = $lon - $radiusLng;
        $maxLng = $lon + $radiusLng;

        //范围
        $range = array(
            'minLat' => $minLat,
            'maxLat' => $maxLat,
            'minLon' => $minLng,
            'maxLon' => $maxLng
        );
        return $range;
    }

}

/**
 * 更具俩点计算距离   m
 * @param type $lng1
 * @param type $lat1
 * @param type $lng2
 * @param type $lat2
 * @return type
 */
function getdistance($lng1, $lat1, $lng2, $lat2) {
    // 将角度转为狐度
    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
    $radLat2 = deg2rad($lat2);
    $radLng1 = deg2rad($lng1);
    $radLng2 = deg2rad($lng2);
    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
    return intval($s);
}

/*
 * $lat1, $lon1: 第一个点的经纬度
 * $lat2, $lon2: 第二个点的经纬度
 * $radius: 可选，默认为半径
 */

function sphere_distance($lat1, $lon1, $lat2, $lon2, $radius = 3000) {
    /*$rad = doubleval(M_PI / 180.0);

    $lat1 = doubleval($lat1) * $rad;
    $lon1 = doubleval($lon1) * $rad;
    $lat2 = doubleval($lat2) * $rad;
    $lon2 = doubleval($lon2) * $rad;

    $theta = $lon2 - $lon1;
    $dist = acos(sin($lat1) * sin($lat2) +
            cos($lat1) * cos($lat2) * cos($theta));
    if ($dist < 0) {
        $dist += M_PI;
    }
    // 单位为 千米
    return $dist = sprintf("%.2f", $dist * $radius);*/
    $earthRadius = 6367000; //approximate radius of earth in meters
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lon1 * pi() ) / 180;
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lon2 * pi() ) / 180;
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
        return round($calculatedDistance)/1000;
}

/**
 * 计算年龄函数
 * @param  $birthday 出生时间 uninx时间戳
 * @param  $time 当前时间
 * */
function getAge($birthday) {
    //格式化出生时间年月日
    $byear = date('Y', $birthday);
    $bmonth = date('m', $birthday);
    $bday = date('d', $birthday);

    //格式化当前时间年月日
    $tyear = date('Y');
    $tmonth = date('m');
    $tday = date('d');

    //开始计算年龄
    $age = $tyear - $byear;
    if ($bmonth > $tmonth || $bmonth == $tmonth && $bday > $tday) {
        $age--;
    }
    return (string) $age;
}

/**
 * RAS返回对应的私钥
 */
function getPrivateKey() {
    $key_file = APP_COMMON . 'rsakey/rsa_private_key.pem';
    $priKey = file_get_contents($key_file);
    //以下为了初始化私钥，保证在您填写私钥时不管是带格式还是不带格式都可以通过验证。
    $priKey = str_replace("-----BEGIN RSA PRIVATE KEY-----", "", $priKey);
    $priKey = str_replace("-----END RSA PRIVATE KEY-----", "", $priKey);
    $priKey = str_replace("\n", "", $priKey);
    $priKey = "-----BEGIN RSA PRIVATE KEY-----" . PHP_EOL . wordwrap($priKey, 64, "\n", true) . PHP_EOL . "-----END RSA PRIVATE KEY-----";
    return openssl_pkey_get_private($priKey);
}

/**
 * RAS返回对应的公钥
 */
function getPublicKey() {
    $key_file = APP_COMMON . 'rsakey/rsa_public_key.pem';
    $pubKey = file_get_contents($key_file);
    //以下为了初始化私钥，保证在您填写私钥时不管是带格式还是不带格式都可以通过验证。
    $pubKey = str_replace("-----BEGIN PUBLIC KEY-----", "", $pubKey);
    $pubKey = str_replace("-----END PUBLIC KEY-----", "", $pubKey);
    $pubKey = str_replace("\n", "", $pubKey);
    $pubKey = "-----BEGIN PUBLIC KEY-----" . PHP_EOL . wordwrap($pubKey, 64, "\n", true) . PHP_EOL . "-----END PUBLIC KEY-----";
    return openssl_pkey_get_public($pubKey);
}

/**
 * RAS公钥加密
 */
function pubEncrypt($content) {
    if (!is_string($content)) {
        return null;
    }
    $eccrypted = '';
    openssl_public_encrypt($content, $eccrypted, getPublicKey()) ? $eccrypted : 'field';
    if ($eccrypted == 'field') {
        return $eccrypted;
    } else {
        return base64_encode($eccrypted);
    }
}

/**
 * RAS私钥解密
 */
function privDecrypt($content) {
    if (!is_string($content)) {
        return null;
    }
    return (openssl_private_decrypt(base64_decode($content), $decrypted, getPrivateKey())) ? $decrypted : 'field';
}
