/*
Navicat MySQL Data Transfer

Source Server         : Demon
Source Server Version : 50558
Source Host           : 47.96.14.234:3306
Source Database       : demon

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2019-01-21 14:18:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) DEFAULT '0' COMMENT '父级菜单id',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名',
  `url` varchar(255) DEFAULT '' COMMENT '链接地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '0', '身份管理', '');
INSERT INTO `admin_menu` VALUES ('2', '1', '管理员列表', '/admin/admin/index');
INSERT INTO `admin_menu` VALUES ('3', '1', '添加管理员', '/admin/admin/add');
INSERT INTO `admin_menu` VALUES ('4', '0', '菜单管理', '');
INSERT INTO `admin_menu` VALUES ('5', '4', '菜单列表', '/admin/menu/index');
INSERT INTO `admin_menu` VALUES ('6', '4', '添加菜单', '/admin/menu/add');

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '身份识别',
  `role` varchar(255) DEFAULT '' COMMENT '权限列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES ('1', '管理员', '*');
INSERT INTO `admin_role` VALUES ('2', '一般用户', '1,2,3,4,5');

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `role_id` int(11) DEFAULT '0' COMMENT '规则id',
  `dateline` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', 'Demon', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-16 14:14:13');
INSERT INTO `admin_user` VALUES ('2', 'admin2', '2', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:51');
INSERT INTO `admin_user` VALUES ('3', 'admin3', '3', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:51');
INSERT INTO `admin_user` VALUES ('4', 'admin4', '4', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:52');
INSERT INTO `admin_user` VALUES ('5', 'admin5', '5', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:53');
INSERT INTO `admin_user` VALUES ('6', 'admin6', '6', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:53');
INSERT INTO `admin_user` VALUES ('7', 'admin7', '7', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:54');
INSERT INTO `admin_user` VALUES ('8', 'admin8', '8', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:55');
INSERT INTO `admin_user` VALUES ('9', 'admin9', '9', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:55');
INSERT INTO `admin_user` VALUES ('10', 'admin10', '10', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:58');
INSERT INTO `admin_user` VALUES ('11', 'admin11', '11', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:30:59');
INSERT INTO `admin_user` VALUES ('12', 'admin12', '12', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:00');
INSERT INTO `admin_user` VALUES ('13', 'admin13', '13', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:01');
INSERT INTO `admin_user` VALUES ('14', 'admin14', '14', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:01');
INSERT INTO `admin_user` VALUES ('15', 'admin15', '15', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:02');
INSERT INTO `admin_user` VALUES ('16', 'admin16', '16', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:03');
INSERT INTO `admin_user` VALUES ('17', 'admin17', '17', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:04');
INSERT INTO `admin_user` VALUES ('18', 'admin18', '18', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:05');
INSERT INTO `admin_user` VALUES ('19', 'admin19', '19', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:07');
INSERT INTO `admin_user` VALUES ('20', 'admin20', '20', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:09');
INSERT INTO `admin_user` VALUES ('21', 'admin21', '21', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '0', '2019-01-15 09:31:10');
INSERT INTO `admin_user` VALUES ('22', 'aaa', 'aaa', 'aaa', '0', null);
INSERT INTO `admin_user` VALUES ('23', 'aaa', 'aaa', 'aaa', '0', null);
INSERT INTO `admin_user` VALUES ('24', 'www', 'www', 'www', '0', null);
INSERT INTO `admin_user` VALUES ('25', 'wwww', 'wwww', 'wwww', '0', null);
INSERT INTO `admin_user` VALUES ('26', 'wwwww', 'wwwww', 'wwwww', '0', null);
INSERT INTO `admin_user` VALUES ('27', 'aaaaa', 'aaaa', 'aaaa', '0', '2019-01-21 09:30:57');
